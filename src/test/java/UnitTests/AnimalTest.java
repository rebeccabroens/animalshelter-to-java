package UnitTests;

import Classes.Animal;
import Classes.Dog;
import Classes.Gender;
import Classes.Reservor;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {
    private Animal testAnimal = new Dog("Hoot hoot", Gender.Female);

    @Test
    void getName() {
        assertEquals("Hoot hoot", testAnimal.getName());
    }

    @Test
    void setPrice(){
        testAnimal.setPrice(50);
        assertEquals(50, testAnimal.getPrice());
    }

    @Test
    void getGender() {
        assertEquals(Gender.Female, testAnimal.getGender());
    }

    @Test
    void getReservedBy() {
        assertEquals(null, testAnimal.getReservedBy());
    }

    @Test
    void getReservedByAlreadyReserved(){
        testAnimal.Reserve("Henk");
        assertEquals("Henk", testAnimal.getReservedBy().getName());
    }

    @Test
    void reserve() {
        assertEquals(true, testAnimal.Reserve("Henk"));
    }

    @Test
    void alreadyReserved(){
        testAnimal.Reserve("Henk");
        assertEquals(false, testAnimal.Reserve("Henk 2"));
    }

    /*
    @Test
    void testToString() {
        assertEquals("Hoot hoot, Female, not reserved", testAnimal.toString());
    }

    @Test
    void testToStringReserved(){
        testAnimal.Reserve("Henk");
        assertEquals("Hoot hoot, Female, reserved by Henk", testAnimal.toString());
    }
     */

}