package UnitTests;

import Classes.Dog;
import Classes.Gender;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;

class DogTest {
    private Dog testDog = new Dog("Risa", Gender.Male);

    @Test
    void getLastWalk() {
        Date today = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        String sToday = formatter.format(today);
        String sLastWalk = formatter.format(testDog.getLastWalk());

        assertEquals(sToday, sLastWalk);
    }

    @Test
    void getNeedsWalk() {
        assertEquals(false, testDog.getNeedsWalk());
    }

    @Test
    void testToString() {
        assertEquals("Risa, Male, not reserved, €0.0, last walk: " + testDog.getLastWalk(), testDog.toString());
    }
}