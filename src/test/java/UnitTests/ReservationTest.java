package UnitTests;

import Classes.Animal;
import Classes.Cat;
import Classes.Dog;
import Classes.Gender;
import org.junit.jupiter.api.Test;
import Main.Reservation;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ReservationTest {
    private Reservation reservations = new Reservation();

    @Test
    void getAnimalsNone() {
        assertEquals(new ArrayList<Animal>(), reservations.getAnimals());
    }

    @Test
    void getAnimals() {
        reservations.NewAnimal(new Cat("Sir Meowsalot", Gender.Male, "Meows a lot"));
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        assertEquals(2, reservations.getAnimals().size());
    }

    @Test
    void newAnimalCat() {
        reservations.NewAnimal(new Cat("Sir Meowsalot", Gender.Male, "Meows a lot"));
        assertEquals(new Cat("Sir Meowsalot", Gender.Male, "Meows a lot").getName(), reservations.getAnimals().get(0).getName());
        assertEquals(new Cat("Sir Meowsalot", Gender.Male, "Meows a lot").getGender(), reservations.getAnimals().get(0).getGender());
    }

    @Test
    void newAnimalDog() {
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        assertEquals(new Dog("Risa", Gender.Male).getName(), reservations.getAnimals().get(0).getName());
        assertEquals(new Dog("Risa", Gender.Male).getGender(), reservations.getAnimals().get(0).getGender());
    }

    @Test
    void calculateAnimalCatPrice() {
        Cat newCat = new Cat("Sir Meowssalot", Gender.Male, "Meows a lot");
        double expectedResult = 130;

        assertEquals(expectedResult, reservations.calculateAnimalPrice(newCat));
    }

    @Test
    void calculateAnimalCatPriceBelowMinimum() {
        Cat newCat = new Cat("Sir Meowssalot", Gender.Male, "Meows a lot, and meows very loudly when he wants food");
        double expectedResult = 35;

        assertEquals(expectedResult, reservations.calculateAnimalPrice(newCat));
    }

    @Test
    void calculateAnimalDogPrice(){
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        reservations.NewAnimal(new Dog("Petey", Gender.Male));
        reservations.NewAnimal(new Dog("Lady", Gender.Female));

        double expectedResult = 400;
        assertEquals(expectedResult, reservations.getAnimals().get(2).getPrice());
    }

    @Test
    void calculateAnimalDogPriceBelowMinimum(){
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        reservations.NewAnimal(new Dog("Petey", Gender.Male));
        reservations.NewAnimal(new Dog("Lady", Gender.Female));
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        reservations.NewAnimal(new Dog("Petey", Gender.Male));
        reservations.NewAnimal(new Dog("Lady", Gender.Female));
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        reservations.NewAnimal(new Dog("Petey", Gender.Male));
        reservations.NewAnimal(new Dog("Lady", Gender.Female));
        reservations.NewAnimal(new Dog("Risa", Gender.Male));
        reservations.NewAnimal(new Dog("Petey", Gender.Male));
        reservations.NewAnimal(new Dog("Lady", Gender.Female));

        double expectedResult = 50;
        assertEquals(expectedResult, reservations.getAnimals().get(10).getPrice());
    }

}