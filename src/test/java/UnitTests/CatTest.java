package UnitTests;

import Classes.Cat;
import Classes.Gender;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CatTest {
    private Cat testCat = new Cat("Sir Meowsalot", Gender.Male, "Meows a lot");

    @Test
    void getBadHabits() {
        assertEquals("Meows a lot", testCat.getBadHabits());
    }

    @Test
    void testToString() {
        assertEquals("Sir Meowsalot, Male, not reserved, €0.0, bad habits: meows a lot", testCat.toString());
    }
}