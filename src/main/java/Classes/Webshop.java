package Classes;

import Interfaces.ISellable;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class Webshop {
    private ArrayList<ISellable> items = new ArrayList<>();

    public Webshop(){
        loadWebshopData();
    }

    public ArrayList<ISellable> getItems(){
        return items;
    }

    public void addItem(ISellable item){
        items.add(item);
        updateWebshopData();
    }

    public void removeItem(ISellable item){
        items.remove(item);
        updateWebshopData();
    }

    private void loadWebshopData(){
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader("src\\\\main\\\\resources\\\\products.json"))
        {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONArray productList = (JSONArray) obj;
            System.out.println(productList);

            //Iterate over product array
            productList.forEach( prod -> parseProductObject( (JSONObject) prod ) );

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseProductObject(JSONObject product)
    {
        JSONObject productObject = (JSONObject) product.get("product");
        items.add(new Product((String) productObject.get("name"), (double) productObject.get("price")));
    }

    private void updateWebshopData(){
        JSONArray productList = new JSONArray();
        for(final ISellable product : getItems()){
            JSONObject jsonProduct = new JSONObject();
            JSONObject jsonProductDetails = new JSONObject();

            jsonProductDetails.put("name", product.Name());
            jsonProductDetails.put("price", product.Price());
            jsonProduct.put("product", jsonProductDetails);

            productList.add(jsonProduct);
        }

        try(FileWriter file = new FileWriter("src\\\\main\\\\resources\\\\products.json")){
            file.write(productList.toJSONString());
            file.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
