package Classes;

public class Cat extends Animal {
    private String BadHabits;

    public String getBadHabits() {
        return BadHabits;
    }

    public Cat(String name, Classes.Gender gender, String badHabits) {
        super(name, gender);
        this.BadHabits = badHabits;
    }

    public String toString(){
        return super.toString() + ", bad habits: " + this.getBadHabits().toLowerCase();
    }

    @Override
    public String Name() {
        return super.getName();
    }

    @Override
    public double Price() {
        return super.getPrice();
    }
}
