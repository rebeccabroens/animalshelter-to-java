package Classes;

import Interfaces.ISellable;

import java.util.Date;

public abstract class Animal implements ISellable {
    private String Name;
    private Gender Gender;
    private Reservor ReservedBy;
    private double Price;

    public String getName() {
        return Name;
    }

    public double getPrice(){
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public Classes.Gender getGender() {
        return Gender;
    }

    public Reservor getReservedBy() {
        return ReservedBy;
    }

    public Animal(String name, Gender gender){
        this.Name   = name;
        this.Gender = gender;
    }

    public Boolean Reserve(String reservedBy){
        if(this.getReservedBy() == null){
            this.ReservedBy = new Reservor(reservedBy, new Date());
            return true;
        }
        return false;
    }

    public String toString(){
        String reserved = "not reserved";
        if(this.getReservedBy() != null){
            return this.Name + ", " + this.Gender + ", reserved by " + this.ReservedBy.getName();
        }
        return this.Name + ", " + this.Gender + ", " + reserved + ", €" + this.getPrice();
    }
}
