package Classes;

import Interfaces.ISellable;

public class Product implements ISellable {
    private String Name;
    private double Price;

    public Product(String name, double price){
        this.Name = name;
        this.Price = price;
    }

    @Override
    public String toString(){
        return Name + ": €" + Price;
    }

    @Override
    public String Name() {
        return Name;
    }

    @Override
    public double Price() {
        return Price;
    }

    @Override
    public void setPrice(double price) {
        this.Price = price;
    }
}