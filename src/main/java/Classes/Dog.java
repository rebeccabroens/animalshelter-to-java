package Classes;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Dog extends Animal {
    private Date LastWalk;

    public Date getLastWalk() {
        return LastWalk;
    }

    public Boolean getNeedsWalk(){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String lastWalk = formatter.format(getLastWalk());
        Date dateNow = new Date();
        String today = formatter.format(dateNow);

        return !today.equals(lastWalk);
    }

    public Dog(String name, Classes.Gender gender) {
        super(name, gender);
        this.LastWalk = new Date();
    }


    public String toString(){
        return super.toString() + ", last walk: " + this.LastWalk;
    }

    @Override
    public String Name() {
        return super.getName();
    }

    @Override
    public double Price() {
        return super.getPrice();
    }
}
