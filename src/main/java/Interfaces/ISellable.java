package Interfaces;

public interface ISellable {
    String Name();
    double Price();

    void setPrice(double price);

}
