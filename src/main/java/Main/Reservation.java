package Main;

import Classes.Animal;
import Classes.Cat;
import Classes.Dog;
import Classes.Gender;

import java.util.ArrayList;

public class Reservation {
    private ArrayList<Animal> Animals = new ArrayList<>();
    private double maxCatPrice = 350;
    private double minCatPrice = 35;
    private double maxDogPrice = 500;
    private double minDogPrice = 50;


    public ArrayList<Animal> getAnimals() {
        return Animals;
    }

    public void NewAnimal(Animal animal){
        animal.setPrice(calculateAnimalPrice(animal));
        Animals.add(animal);
    }

    private int getDogCount() {
        return (int) this.Animals.stream().filter(animal -> animal.getClass() == Dog.class).count();
    }

    private double calculateCatPrice(Cat cat){
        double priceCat = maxCatPrice;

        for(int i = 0; i < cat.getBadHabits().length(); i++){
            priceCat -= 20;
        }

        if(priceCat < minCatPrice){
            priceCat = minCatPrice;
        }

        return priceCat;
    }

    private double calculateDogPrice(){
        double price = maxDogPrice;

        for (int i = 0; i < getDogCount(); i++) {
            price -= 50;
        }

        if(price < minCatPrice){
            price = minDogPrice;
        }
        return price;
    }

    public double calculateAnimalPrice(Animal animal){
        if(animal.getClass().equals(Cat.class)){
            return calculateCatPrice((Cat) animal);
        }
        else if(animal.getClass().equals(Dog.class)){
            return calculateDogPrice();
        }

        return 0;
    }
}
