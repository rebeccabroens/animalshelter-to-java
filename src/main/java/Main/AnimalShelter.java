package Main;

import Classes.Cat;
import Classes.Dog;
import Classes.Gender;

import java.util.Scanner;

public class AnimalShelter {
    public static void main(String[] args){
        Reservation reservations = new Reservation();
        while (true){
            int Action = ChooseAction();

            switch (Action){
                //Add animal
                case 1:
                    AddAnimal(reservations);
                    break;
                //Get List
                case 2:
                    System.out.print(reservations.getAnimals());
                    break;
                //Reserve
                case 3:
                    break;
            }
        }
    }

    public static int ChooseAction(){
        Scanner scan = new Scanner(System.in);

        System.out.println("Choose an option below: \n" +
                "1: Add a new animal \n" +
                "2: Get a list of all animals \n" +
                "3: Reserve an animal");

        int ChooseAction = scan.nextInt();
        return ChooseAction;
    }

    public static String GetSpecies(){
        String Answer = "";
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.println("Alright, let's see. Would you like to add a cat or dog?");
            Answer = scan.next().toLowerCase();

            if (Answer.equals("dog")) {
                break;
            }

            if(Answer.equals("cat")){
                break;
            }
        }

        return Answer;
    }

    public static String GetNameAnimal(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Next up, what's their name?");
        return scan.nextLine();
    }

    public static Gender GetGenderAnimal(){
        Gender returnGender;
        Scanner scan = new Scanner(System.in);

        while (true) {
            System.out.println("Is it a male or female?");
            String Answer = scan.next().toLowerCase();

            if (Answer.equals("male")) {
                returnGender = Gender.Male;
                break;
            }

            if(Answer.equals("female")){
                returnGender = Gender.Female;
                break;
            }
        }
        return returnGender;
    }

    public static String GetBadHabits(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Lastly, what kind of mischievous things does the cat get up to?");
        return scan.nextLine();
    }

    public static void AddAnimal(Reservation reservation){
        String species  = GetSpecies();
        String name     = GetNameAnimal();
        Gender gender   = GetGenderAnimal();

        if(species.equals("cat")){
            String badHabits = GetBadHabits();
        reservation.NewAnimal(new Cat(name, gender, badHabits));
        }
        else if(species.equals("dog")){
            reservation.NewAnimal(new Dog(name, gender));
        }
    }
}
