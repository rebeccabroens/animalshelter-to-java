package GUI;

import Classes.*;
import Interfaces.ISellable;
import Main.Reservation;
import javafx.application.Application;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.FileWriter;
import java.io.IOException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

public class AnimalShelterGUI extends Application {

    Gender genderChosen;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        TextField tfHabits = new TextField();
        Reservation reservation = new Reservation();
        Webshop webshop = new Webshop();
        primaryStage.setTitle("Animal Shelter");

        //Controls layout
        String animals[] ={ "Dog", "Cat"};
        ComboBox cbSpecies = new ComboBox(FXCollections.observableArrayList(animals));
        cbSpecies.getSelectionModel().selectedItemProperty().addListener( (options, oldValue, newValue) -> {
                    if (newValue.equals("Cat")) {
                        tfHabits.setDisable(false);
                    } else {
                        tfHabits.setDisable(true);
                    }
                }
        );

        VBox vName = new VBox();
        Label nameTitle = new Label("Name:");
        TextField tfName = new TextField();
        vName.getChildren().addAll(nameTitle, tfName);

        String gender[] ={ "Male", "Female"};
        ComboBox cbGender = new ComboBox(FXCollections.observableArrayList(gender));
        cbGender.getSelectionModel().selectedItemProperty().addListener( (options, oldValue, newValue) -> {
                    if (newValue.equals("Male")) {
                        genderChosen = Gender.Male;
                    } else {
                        genderChosen = Gender.Female;
                    }
                }
        );

        VBox vHabits = new VBox();
        Label nameBadHabits = new Label("Bad habits:");
        tfHabits.setDisable(false);
        vHabits.getChildren().addAll(nameBadHabits, tfHabits);

        Button btnAddAnimal = new Button();
        btnAddAnimal.setText("Add new animal");

        VBox layoutControls = new VBox(10);
        layoutControls.getChildren().addAll(cbSpecies, vName, cbGender, vHabits, btnAddAnimal);
        //

        //VBox Reserve
        VBox vbReserve = new VBox();
        Label titleReserve = new Label("Reserved by:");
        TextField tfReservorName = new TextField();
        Button btnReserve = new Button();
        btnReserve.setText("Reserve Animal");
        vbReserve.getChildren().addAll(titleReserve, tfReservorName, btnReserve);
        //

        //VBox item name
        VBox vbItemName = new VBox();
        Label titleName = new Label("Item name:");
        TextField tfItemName = new TextField();
        vbItemName.getChildren().addAll(titleName, tfItemName);
        //


        //VBox item price
        VBox vbItemPrice = new VBox();
        Label titlePrice = new Label("Item price:");
        TextField tfPrice = new TextField();
        Pattern pattern = Pattern.compile("\\d*|\\d+\\.\\d*");
        TextFormatter formatter = new TextFormatter((UnaryOperator<TextFormatter.Change>) change -> {
            return pattern.matcher(change.getControlNewText()).matches() ? change : null;
        });
        tfPrice.setTextFormatter(formatter);
        vbItemPrice.getChildren().addAll(titlePrice, tfPrice);
        //


        //HBox add item
        HBox addItembox = new HBox(10);
        addItembox.getChildren().addAll(vbItemName, vbItemPrice);
        //


        //Webshop buttons
        HBox webshopButtons = new HBox(10);
        Button btnAddItem = new Button("Add Item");
        Button btnRemoveItem = new Button("Remove Item");
        webshopButtons.getChildren().addAll(btnAddItem, btnRemoveItem);
        //


        //List animals layout
        ObservableList<Animal> lstAnimals = FXCollections.<Animal>observableArrayList(reservation.getAnimals());
        ListView<Animal> lvAnimals = new ListView<>(lstAnimals);
        lstAnimals.addListener(new ListChangeListener<Animal>() {
            @Override
            public void onChanged(ListChangeListener.Change<? extends Animal> c) {
                while (c.next()) {
                    for (Animal addedItem : c.getAddedSubList()) {
                        System.out.println("Item: " + addedItem.toString());
                    }

                }
            }
        });
        lvAnimals.setOrientation(Orientation.VERTICAL);
        lvAnimals.setMinWidth(450);
        //


        //List shop items layout
        ObservableList<ISellable> lstShopItems = FXCollections.<ISellable>observableArrayList(webshop.getItems());
        ListView<ISellable> lvShopItems = new ListView<>(lstShopItems);
        lvShopItems.setOrientation(Orientation.VERTICAL);
        lvShopItems.setMinWidth(250);
        //


        //Grid layout
        GridPane layoutAll = new GridPane();
        layoutAll.setHgap(20);
        layoutAll.setVgap(10);
        layoutAll.setPadding(new Insets(15, 20, 15, 20));
        layoutAll.add(layoutControls, 0,0);
        layoutAll.add(lvAnimals, 1,0);
        layoutAll.add(vbReserve, 1,1);
        layoutAll.add(lvShopItems, 2,0);
        layoutAll.add(addItembox, 2,1);
        layoutAll.add(webshopButtons, 2,2);
        //

        Scene scene = new Scene(layoutAll, 930, 500);
        primaryStage.setScene(scene);

        //Button event
        btnAddAnimal.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Gender gender = genderChosen;
                String name;
                String badHabits;

                // Get name:
                name = tfName.getText();

                // Get bad habits
                badHabits = tfHabits.getText();

                if (cbSpecies.getValue().equals("Dog")) {
                    reservation.NewAnimal(new Dog(name, gender));
                }
                else if (cbSpecies.getValue().equals("Cat")) {
                    reservation.NewAnimal(new Cat(name, gender, badHabits));
                }

                lstAnimals.setAll(reservation.getAnimals());
            }
        });


        btnAddItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                webshop.addItem(new Product(tfItemName.getText(), Double.parseDouble(tfPrice.getText())));
                lstShopItems.setAll(webshop.getItems());
            }
        });

        btnRemoveItem.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                webshop.removeItem(lvShopItems.getSelectionModel().getSelectedItem());
                lstShopItems.setAll(webshop.getItems());
            }
        });

        btnReserve.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                lvAnimals.getSelectionModel().getSelectedItem().Reserve(tfReservorName.getText());
                lstAnimals.setAll(reservation.getAnimals());
            }
        });
        //

        primaryStage.show();
    }
}

